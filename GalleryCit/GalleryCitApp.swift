//
//  GalleryCitApp.swift
//  GalleryCit
//
//  Created by Oktavia Citra on 05/11/21.
//

import SwiftUI

@main
struct GalleryCitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
